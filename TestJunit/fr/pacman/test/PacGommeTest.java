package fr.pacman.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.pacman.zonedejeux.point.PacGomme;

class PacGommeTest {

	@Test
	public void seFaitManger() {
		PacGomme pac = new PacGomme(10, 3);
		
		assertEquals(10, pac.seFaitManger());		 

	}

}
