package fr.pacman.background.partie;

/**
 * La classe Partie est probablement l'une des plus importe car elle gere du lancement jusqu'a la fin d'une 
 * partie lancer, elle regroupe l'ensemble des personnages avec les decors et permet leurs interactions.
 * @author mathe
 * Elle affiche aussi les stats de la partie avec le temps de jeu, le niveau jouer ect...
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import fr.exceptionpersonnalisee.BlockException;
import fr.exceptionpersonnalisee.ModificationNiveauException;
import fr.pacman.background.gestion.FichierCharger;
import fr.pacman.main.Main;
import fr.pacman.main.MenuPause;
import fr.pacman.zonedejeux.acteur.Bashful;
import fr.pacman.zonedejeux.acteur.Fantome;
import fr.pacman.zonedejeux.acteur.PacMan;
import fr.pacman.zonedejeux.acteur.Pockey;
import fr.pacman.zonedejeux.acteur.Shadow;
import fr.pacman.zonedejeux.acteur.Speedy;
import fr.pacman.zonedejeux.decor.Block;
import fr.pacman.zonedejeux.decor.Niveau;
import fr.pacman.zonedejeux.decor.Record;
import fr.pacman.zonedejeux.point.PacGomme;
import fr.pacman.zonedejeux.point.PacGommeFruit;
import fr.pacman.zonedejeux.point.SuperPacGomme;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;

@SuppressWarnings("serial")
public class Partie extends JPanel implements ActionListener, KeyListener {

	public static final int OK = 0;
	public static final int ERROR1 = -1;
	public static final int ERROR2 = -2;
	
	private long heureDepart;
	private long heurePause;
	private long heureRedemarage;
	private long heureDernierImages;
	private long tempsModeSuper;
	private int fps;

	private Collection<Integer> tempsImages = new ArrayList<>();
	private Font fontCentre;
	private Font fontTexte;
	private Font fontTitre;
	private int score = 0;
	private boolean fin;
	public static boolean start;
	private Niveau niveauJouer;
	private PacMan pacMan;
	private long tempsJeux;
	final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	private Collection<PacGomme> pacgommes;
	private Collection<PacGommeFruit> pacgommesfruitManger;
	private ArrayList<Fantome> fantomes;
	public static JFXPanel fxpanel = new JFXPanel();

	public static Timer timer;
	public Partie() {
		super();
	}

	/**
	 * Constructeur Partie.
	 * @param idNiveau
	 */
	public Partie(String idNiveau) {
		setLayout(null);
		this.setPreferredSize(new Dimension(dim.width, dim.height));
		this.setBackground(Color.black);
		this.setFocusable(true);
		initialisation(idNiveau);
		addKeyListener(this);
	}

	/**
	 * Cette methode Inistialise le debut d'une Partie, avec le chargement des fichiers necessaire.
	 * @param idNiveau
	 */
	public int initialisation(String idNiveau) {

		int result = OK;
		try {
			fontTexte = Font.createFont(Font.TRUETYPE_FONT, new File("data/assets/fonts/emulogic.ttf")).deriveFont(15f);
			fontCentre = Font.createFont(Font.TRUETYPE_FONT, new File("data/assets/fonts/emulogic.ttf"))
					.deriveFont(100f);
			fontTitre = Font.createFont(Font.TRUETYPE_FONT, new File("data/assets/fonts/PAC-FONT.TTF"))
					.deriveFont(100f);
		} catch (Exception e) {
			result = ERROR1;
			e.printStackTrace();
		}

		niveauJouer = FichierCharger.getUnNiveau(idNiveau);

		new Dijkstra(this.niveauJouer.getMap());
		try {
			niveauJouer.getMap().orientationMurMap();
		} catch (BlockException e) {
			result = ERROR2;
			e.printStackTrace();
		}
		

		
		Scene scenefx = new MenuPause().getScene();
		fxpanel.setVisible(false);
		fxpanel.setScene(scenefx);
		this.setComponentZOrder(fxpanel, 0);
		add(fxpanel, BorderLayout.CENTER);
		pacMan = new PacMan(15, 10);
		pacgommes = creationDesPacGomme();
		restart();
		affichageTitre();
		heureDepart = System.currentTimeMillis();
		tempsJeux = heureDepart;
		fin = false;
		timer = new Timer(10, this);
		timer.start();
		
		return result;
	}
	
	public static void ToggleMenu(Boolean b) {
		fxpanel.setVisible(b);
		fxpanel.setFocusable(b);
	}

	/**
	 * Cette methode permet de restart une partie deja lancer.
	 */
	private void restart() {
		start = false;

		if (pacMan.getVieRestante() >= 0 && pacgommes.size() > 0) {
			try {
				pacMan.setCoordonner(15, 10);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pacMan.setOrientation(3);
			pacMan.changementImages();
			fantomes = new ArrayList<>(Arrays.asList(new Speedy(), new Shadow(), new Bashful(), new Pockey()));
		} else {
			fin = true;
		}
		heurePause = System.currentTimeMillis();
	}

	/**
	 * @return l'ensemble des pacGomes qui doivent figurer dans la partie.
	 */
	private Collection<PacGomme> creationDesPacGomme() {
		pacgommes = new ArrayList<>();
		pacgommesfruitManger = new ArrayList<>();

		for (Block i : niveauJouer.getMap().getMapCollection()) {
			try {
				switch (i.getTypeblock()) {
				case 2:
					pacgommes.add(new PacGomme(i.getCoordonnerX(), i.getCoordonnerY()));
					break;
				case 3:
					pacgommes.add(new SuperPacGomme(i.getCoordonnerX(), i.getCoordonnerY()));
					break;
				case 4:
					pacgommes.add(new PacGommeFruit(i.getCoordonnerX(), i.getCoordonnerY()));
					break;

				default:
					break;
				}

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		return pacgommes;
	}

	/**
	 * Affiche le titre PACMAN au dessus de la zone de jeu.
	 */
	private void affichageTitre() {
		JLabel titre = new JLabel("<html><font color='yellow'>PAC</font><font color='red'>man</font></html>", JLabel.CENTER);
        this.setLayout(new FlowLayout());
        titre.setFont(fontTitre);
        add(titre); 
	}

	/**
	 * Affiche les contoures du jeu lors du lancement d'une partie.
	 * @param g
	 */
	private void affichageContour(Graphics g) {
		g.setFont(this.fontTexte);
		g.setColor(Color.white);

		g.drawString("Vie restante : " + pacMan.getVieRestante(),
				Main.tailleJeuxX + ((this.getWidth() - Main.tailleJeuxX) / 2),
				30 + ((this.getHeight() - Main.tailleJeuxY) / 2));
		for (int i = 0; i < pacMan.getVieRestante(); i++) {
			g.drawImage(new ImageIcon("./data/assets/images/PAC-MAN.png").getImage(),
					28 * i + Main.tailleJeuxX + ((this.getWidth() - Main.tailleJeuxX) / 2),
					35 + ((this.getHeight() - Main.tailleJeuxY) / 2), null);
		}

		g.drawString("Pacgomme Fruit mange : " + pacgommesfruitManger.size(),
				Main.tailleJeuxX + ((this.getWidth() - Main.tailleJeuxX) / 2),
				80 + ((this.getHeight() - Main.tailleJeuxY) / 2));

		for (int i = 0; i < pacgommesfruitManger.size(); i++) {
			g.drawImage(new ImageIcon("./data/assets/images/pacgommefruit.png").getImage(),
					28 * i + Main.tailleJeuxX + ((this.getWidth() - Main.tailleJeuxX) / 2),
					85 + ((this.getHeight() - Main.tailleJeuxY) / 2), null);
		}

		tempsImages.add((int) (System.currentTimeMillis() - heureDernierImages));
		if (tempsImages.size() > 20) {
			this.fps = 0;
			for (Integer i : tempsImages) {
				fps += i;
			}
			this.fps = (int) (1 / (fps / (tempsImages.size() * 1000.0)));
			tempsImages.clear();
		}

		g.drawString("Score : " + score, this.getWidth() / 2 - Main.tailleJeuxX / 2 - 220,
				30 + ((this.getHeight() - Main.tailleJeuxY) / 2));

		g.drawString("Temps : " + (float) Math.round(this.getTempsJeux() / 100) / 10 + "s",
				this.getWidth() / 2 - Main.tailleJeuxX / 2 - 220,
				30 + ((this.getHeight() - Main.tailleJeuxY) / 2) + 30);

		g.drawString("FPS " + fps, 0, g.getFont().getSize());

		g.drawString("Record : " + niveauJouer.getRecordNiveau().getNom(),
				this.getWidth() / 2 - Main.tailleJeuxX / 2 - 220,
				30 + ((this.getHeight() - Main.tailleJeuxY) / 2) + 80);
		g.drawString("Score : " + niveauJouer.getRecordNiveau().getMeilleurScrore(),
				this.getWidth() / 2 - Main.tailleJeuxX / 2 - 220,
				30 + ((this.getHeight() - Main.tailleJeuxY) / 2) + 100);
		g.drawString("Temps : " + (float) Math.round(niveauJouer.getRecordNiveau().getMeilleurTemps() / 100) / 10 + "s",
				this.getWidth() / 2 - Main.tailleJeuxX / 2 - 220,
				30 + ((this.getHeight() - Main.tailleJeuxY) / 2) + 120);
	}

	/**
	 * Permet d'afficher le centre du jeu.
	 * @param g
	 * @param text
	 */
	private void affichecenter(Graphics g, String text) {

		FontMetrics metrics = g.getFontMetrics(this.fontCentre);
		Rectangle rect = new Rectangle(this.getWidth(), this.getHeight());
		int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
		int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
		g.setColor(Color.white);
		g.setFont(this.fontCentre);
		g.drawString(text, x, y);
	}

	/**
	 * Affiche les stats de la partie en cours.
	 * @param g
	 */
	private void affichageDecompte(Graphics g) {
		if (!fin) {
			if (Math.round((System.currentTimeMillis() - heurePause) / 100) / 10 < 3) {
				affichecenter(g, "" + (3 - Math.round((System.currentTimeMillis() - heurePause) / 100) / 10));

			} else if (Math.round((System.currentTimeMillis() - heurePause) / 100) / 10 == 3) {
				affichecenter(g, "GO");
				if (!start) {
					start = true;
					heureRedemarage = System.currentTimeMillis();
					tempsJeux += (heureRedemarage - heurePause);
				}
			}
		} else if (pacMan.getVieRestante() < 0) {
			affichecenter(g, "PERDU");
			start = false;
			
		} else {
			affichecenter(g, "victoire");
			start = false;
		}
	}

	/**
	 * Cette methode gere la supression des elements 
	 * graphique lorsque ils peuvent manger c'est dernier.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		PacGomme faitMangerP = null;

		super.paintComponent(g);

		for (Block i : niveauJouer.getMap().getMapCollection()) {
			try {
				i.afficher(g, this.getWidth(), this.getHeight());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (PacGomme i : pacgommes) {
			try {
				i.afficher(g, this.getWidth(), this.getHeight());
				if (i.touche(pacMan)) {
					score += i.seFaitManger();
					faitMangerP = i;
					if (i.getClass() == SuperPacGomme.class) {
						pacMan.setModeSuper(true);
						tempsModeSuper = System.currentTimeMillis();
						for (Fantome j : fantomes) {
							j.peur();
						}

					} else if (i.getClass() == PacGommeFruit.class) {
						pacgommesfruitManger.add((PacGommeFruit) i);
					}
				}

			} catch (Exception e) {

				e.printStackTrace();
			}
		}

		pacgommes.remove(faitMangerP);

		for (Fantome i : fantomes) {
			try {

				i.afficher(g, this.getWidth(), this.getHeight());
				
				if (pacMan.getVieRestante() >= 0) {
					if (i.touche(pacMan)) {
						if (pacMan.isModeSuper()) {
							this.score += i.seFaitManger();
							if(i.getClass().toString().contains("Speedy")) {
								Fantome tmp = new Speedy();
								tmp.peur();
								fantomes.set(fantomes.indexOf(i), tmp);
							}else if(i.getClass().toString().contains("Pockey")) {
								Fantome tmp = new Pockey();
								tmp.peur();
								fantomes.set(fantomes.indexOf(i), tmp);
							}else if(i.getClass().toString().contains("Bashful")) {
								Fantome tmp = new Bashful();
								tmp.peur();
								fantomes.set(fantomes.indexOf(i), tmp);
							} else {
								fantomes.remove(i);
							}
						} else {
							pacMan.perdVie();
							restart();
						}

					}

				}
			} catch (Exception e) {

				e.printStackTrace();
			}

		}

		if (pacMan.isModeSuper()) {
			if (Math.round((System.currentTimeMillis() - tempsModeSuper) / 100) / 10 >= 6) {
				pacMan.setModeSuper(false);
				for (Fantome i : fantomes) {
					i.setTexture(i.remetreTexture());
				}
			}
		}

		try {
			pacMan.afficher(g, this.getWidth(), this.getHeight());

		} catch (Exception e) {
			e.printStackTrace();
		}
		affichageContour(g);
		affichageDecompte(g);
		
		if(fin && (Math.round((System.currentTimeMillis() - heurePause) / 100) / 10 >= 3)) {
			Platform.runLater(new Runnable() {
				public void run() {
					Main.quitter();
				}
			});
			
		}
		heureDernierImages = System.currentTimeMillis();
	}

	/**
	 * Cette methode afficher le nombre de FPS durant la partie.
	 */
	public void actionPerformed(ActionEvent e) {
		if (start) {
			try {
				pacMan.deplacement(niveauJouer.getMap());

			} catch (Exception e1) {
				System.out.println(e1);

			}

			for (Fantome i : fantomes) {
				try {
					i.deplacement(pacMan, niveauJouer.getMap());

				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}

			if (Math.round((System.currentTimeMillis() - heureRedemarage) / 100) / 10 >= 3
					&& pacMan.getVieRestante() >= 0) {
				for (Fantome i : fantomes) {
					i.setLibre(true);
				}
			}

			if (pacgommes.size() <= 0) {
				heurePause = System.currentTimeMillis();
				fin = true;
				try {
					niveauJouer.setRecordNiveau(new Record(FichierCharger.getJoueurConecter().getId(),FichierCharger.getJoueurConecter().getNom(), score,(int) this.getTempsJeux()));
				} catch (ModificationNiveauException e1) {
					
					e1.printStackTrace();
				}
				try {
					FichierCharger.modifierNiveau(niveauJouer);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}

		for (int i = 0; i < Dijkstra.sommet.size(); i++) {
			if (pacMan.touche(Dijkstra.sommet.get(i))) {
				pacMan.setUpdaiteDernierSommet(i);
			}
		}
		

		
		repaint();
	}

	/**
	 * methode appel� lors de l'interception de touches, permet d'appeler une
	 * fonction de pacmn pour lui definir sa nouvelle trajectoire
	 * 
	 * @param orient
	 * @throws BlockException
	 */
	public void mouvementPacMan(int orient) {
		pacMan.SetFutureOrientation(orient);
	}

	/**
	 * @return le temps actuel de la partie.
	 */
	private long getTempsJeux() {
		if (start) {
			return System.currentTimeMillis() - this.tempsJeux;
		}
		return heurePause - this.tempsJeux;

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// System.out.println(e);

	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_RIGHT) {
			this.mouvementPacMan(3);
		} else if (key == KeyEvent.VK_LEFT) {
			this.mouvementPacMan(1);
		} else if (key == KeyEvent.VK_DOWN) {
			this.mouvementPacMan(4);
		} else if (key == KeyEvent.VK_UP) {
			this.mouvementPacMan(2);
			// rotateImage(panel.getImPacMan(),90.0F);
		} else if (key == KeyEvent.VK_ESCAPE) {
			if(start == true) {
				start= false;
				timer.stop();
				ToggleMenu(true);
				this.setFocusable(false);
				fxpanel.setFocusable(true);
			}else if(start == false) {
				ToggleMenu(false);
				fxpanel.setFocusable(false);
				this.setFocusable(true);
				fxpanel.setFocusable(false);
				start= true;
				timer.start();
			}
		}
	}
	
	public Niveau getNiveauJouer() {
		return niveauJouer;
	}
	
	public JFXPanel getJFXPanel() {
		return fxpanel;
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

}