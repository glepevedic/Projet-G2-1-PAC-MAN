package fr.pacman.background.gestion;

import fr.pacman.zonedejeux.decor.Joueur;
import fr.pacman.zonedejeux.decor.Map;
import fr.pacman.zonedejeux.decor.Niveau;
import fr.pacman.zonedejeux.decor.Record;

/**
 * 
 * @author gilli
 *
 */
public class NouveauFichier {

	/**
	 * Cette class cr�� un niveau avec un ID unique. un nomfichier niveau"ID".pac
	 * Puis le niveau est enregister son fichier. pour finir le fichier et charger
	 * dans fichierCharger.
	 * 
	 * @return retourne le niveau cr�e
	 * @throws Exception
	 */
	public static Niveau nouveauNiveau(String nom) {
		Niveau nouveauNiveau = null;
		String nomFichier = null;
		String nouvelleIdString = null;
		Map map = null;
		Record record = null;

		nouvelleIdString = FichierCharger.plusGrandIdMap();
		int nouvelleIdInt = Integer.parseInt(nouvelleIdString.replace('N', '0')) + 1;
		nomFichier = "data/niveau/niveau" + nouvelleIdInt + ".pac";
		nouvelleIdString = ('N' + String.format("%010d", nouvelleIdInt));

		map = FichierCharger.getUnNiveau("N0000000000").getMap();
		record = new Record("J0000000000");

		
		nouveauNiveau = new Niveau(nomFichier, nouvelleIdString,nom, map,record);
		
		LectureFichierXML.ecrireNiveau(nouveauNiveau);

		try {
			FichierCharger.chargerNiveau(nomFichier);
		} catch (Exception e) {

			e.printStackTrace();
		}

		return FichierCharger.getUnNiveau(nouvelleIdString);
	}

	/**
	 * Cette class cr�� un jouer avec un ID aleatoire. un nomfichier
	 * joueur"nomJoueur".pac Puis ajoute le niveau fraichement cr�e dans le
	 * fichierCharger. pour finir il retourne le niveau.
	 * 
	 * @return retourne le niveau cr�e
	 * @throws Exception
	 */
	public static Joueur nouveauJoueur(String nomJoueur) {
		String id = "J" + String.format("%010d", Math.round((Math.random() * Math.pow(10, 10))));
		String nomFichier = "./fic/joueur" + nomJoueur + ".pac";

		Joueur nouveauJoueur = new Joueur(nomFichier, id, nomJoueur);

		LectureFichierXML.ecrireJoueur(nouveauJoueur);
		try {
			FichierCharger.changementJoueur(nomFichier);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return FichierCharger.getJoueurConecter();
	}

}
