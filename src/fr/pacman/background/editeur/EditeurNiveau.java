package fr.pacman.background.editeur;

import fr.pacman.zonedejeux.decor.Niveau;
/**
 * 
 * @author gilli
 *
 */
public class EditeurNiveau {
	private EditeurMap editeurMap;
	private Niveau niveau;
	
	/**
	 * Constructeur niveau
	 * @param niveau
	 */
	public EditeurNiveau(Niveau niveau) {
		this.niveau = niveau;
	}
	
	/**
	 * Cette methode permet de renomer les niveaux.
	 * @param nom
	 */
	public void renommerNiveau(String nom) {
		this.niveau.setNom(nom);
	}
	
	/**
	 * Cette methode permet de modifier la map
	 * @return
	 * @throws Exception
	 */
	public EditeurMap modifierMap() throws Exception {
	//	this.editeurMap =new EditeurMap(this.niveau.getMap());
		return this.editeurMap;
	}
	
	/**
	 * Cette methode permet de fermer d'editeur de niveau.
	 * @return
	 * @throws Exception
	 */
	public Niveau fermerEditeur() throws Exception{
		this.niveau.setMap(editeurMap.finModificationMap());
		return this.niveau;
	}

}
