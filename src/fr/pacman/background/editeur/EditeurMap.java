package fr.pacman.background.editeur;

/**
 * Cette classe est l'affiche graphique des elements definis dans la classe
 * EditeurM.
 * @author mathe
 */

import java.util.ArrayList;
import java.util.Collection;

import fr.exceptionpersonnalisee.BlockException;
import fr.pacman.background.gestion.FichierCharger;
import fr.pacman.zonedejeux.decor.Block;
import fr.pacman.zonedejeux.decor.Map;
import fr.pacman.zonedejeux.decor.Niveau;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class EditeurMap extends Stage {
	private Block[][] mapBlock;
	private Button save = new Button("Sauvegarder");
	private Button retour = new Button("Retour");
	private Button bmur = new Button("Mur");
	private Button bcouloir = new Button("Couloir");
	private Button bpg = new Button("Super PackGomme");
	private Button bpgf = new Button("PackGomme Fruit");
	private Label titre	= new Label("Choissisez un bloc a placer");
	private int typebloc = 0;
	private ImageView[][] imglist = new ImageView[Map.largeurMap][Map.longueurMap];
	private static Niveau niveauJouer;
	
	/**
	 * Constructeur Editeur Map.
	 * @param idNiveau
	 * @throws BlockException
	 * @throws FileNotFoundException
	 */
	public EditeurMap(String idNiveau) throws BlockException, FileNotFoundException {
		niveauJouer = FichierCharger.getUnNiveau(idNiveau);
		this.mapBlock = Map.convertirColectionToTab(niveauJouer.getMap().getMapCollection());

		//this.setFullScreen(true);
		this.setTitle("PAC-MAN");
		Scene scene = new Scene(creerContenue());
		this.setScene(scene);
	}
	
	/**
	 * @return le contenu de la scene.
	 * @throws BlockException
	 * @throws FileNotFoundException
	 */
	private Parent creerContenue() throws BlockException, FileNotFoundException {
		BorderPane root = new BorderPane();
		VBox listeitem = new VBox();
		HBox listebouton = new HBox();
		GridPane editeur = new GridPane();
		listebouton.getChildren().addAll(retour,save);
		listeitem.getChildren().addAll(titre,bmur, bcouloir, bpg,bpgf);
		for(int i = 0; i<Map.largeurMap;i++) {
			for(int j = 0;j<Map.longueurMap;j++) {
				imglist[i][j] = new ImageView(new Image(new FileInputStream(Block.getBlockUrlWithType(mapBlock[i][j].getTypeblock()))));
				int jj = j;
				int ii = i;
				imglist[i][j].addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
					try {
						gererclickimg(ii,jj);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				});
				editeur.add(imglist[i][j], j, i);
			}
		}
		save.setOnAction(e -> gererClic(e,"save"));
		retour.setOnAction(e -> gererClic(e,"retour"));
		bmur.setOnAction(e -> gererClic(e,"bmur"));
		bcouloir.setOnAction(e -> gererClic(e,"bcouloir"));
		bpg.setOnAction(e -> gererClic(e,"bpg"));
		bpgf.setOnAction(e -> gererClic(e,"bpgf"));
		root.setBottom(listebouton);
		root.setCenter(editeur);
		root.setLeft(listeitem);
		root.setStyle("-fx-background : black");
		return root;
	}
	
	/**
	 * Cette methode ajoute le type de bloc voulu en fonction du numero
	 * ecris (entre 0 et 3).
	 * @param i
	 * @param j
	 * @throws Exception
	 */
	public void gererclickimg(int i, int j) throws Exception {
		blockInterditModif(j, i);
		imglist[i][j].setImage(new Image(new FileInputStream(Block.getBlockUrlWithType(this.typebloc))));
		if(typebloc == 0) {
			this.ajouterMur(j, i);
		} else if(typebloc == 2) {
			this.ajouterCouloir(j, i);
		} else if(typebloc == 3) {
			this.ajouterSuperPacGomme(j, i);
		} else if(typebloc == 4) {
			this.ajouterPacGommeFruit(j, i);
		}
		
	}

	/**
	 * Cette methode gere les clicks sur l'editeur pour placer les blocks.
	 * @param e
	 * @param label
	 */
	public void gererClic(ActionEvent e, String label) {
		if(label == "bmur") {
			this.typebloc = 0;
		} else if (label == "bcouloir") {
			this.typebloc = 2;
		} else if (label == "bpg") {
			this.typebloc = 4;
		} else if (label == "bpgf") {
			this.typebloc = 3;
		} else if (label == "save") {
			try {
				finModificationMap();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (label == "retour") {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Retour au menu");
			alert.setHeaderText("Etes vous sur de vouloir faire retour sans sauvegarder");
			Optional<ButtonType> option = alert.showAndWait();
			if (option.get() == ButtonType.OK) {
				//Retour au menu
			}
		}
	}
	
	/**
	 * Cette methode ajoute un mur.
	 * @param x
	 * @param y
	 * @throws Exception
	 */
	public void ajouterMur(int x, int y) throws Exception {
		this.mapBlock[y][x] = Block.newBlockMur(x, y);
	}

	/**
	 * Cette methode ajoute un couloir.
	 * @param x
	 * @param y
	 * @throws Exception
	 */
	public void ajouterCouloir(int x, int y) throws Exception {
		this.mapBlock[y][x] = Block.newBlockCouloir(x, y);
	}

	/**
	 * Cette methode ajoute un SuperPacGomme.
	 * @param x
	 * @param y
	 * @throws Exception
	 */
	public void ajouterSuperPacGomme(int x, int y) throws Exception {
		this.mapBlock[y][x] = Block.newBlockSuperPacGomme(x, y);
	}

	/**
	 * Cette methode ajoute un PacGommeFruit.
	 */
	public void ajouterPacGommeFruit(int x, int y) throws Exception {
		this.mapBlock[y][x] = Block.newBlockPacGommeFruit(x, y);
	}

	/**
	 * Cette methode definis les blocks de type spawn que l'on ne
	 * peux pas modifier.
	 * @param x
	 * @param y
	 * @throws Exception
	 */
	public void blockInterditModif(int x, int y) throws Exception {
		if (this.mapBlock[y][x].getTypeblock() == Block.typeSpawn) {
			throw new Exception("Erreur : Block spawn");
		}

	}

	/**
	 * Cette methode permet de sauvegarder les motifs de la map.
	 * @return
	 * @throws Exception
	 */
	public Map finModificationMap() throws Exception {
		Map map = new Map();
		Collection<Block> mapCollection = new ArrayList<>();

		for (int largeur = 0; largeur < Map.largeurMap; largeur++) {
			for (int longeur = 0; longeur < Map.longueurMap; longeur++) {

				mapCollection.add(this.mapBlock[largeur][longeur]);
			}
		}
		map.setMapCollection(mapCollection);
		niveauJouer.setMap(map);
		FichierCharger.modifierNiveau(niveauJouer);
		return map;
	}
}
