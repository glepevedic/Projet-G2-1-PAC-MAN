package fr.pacman.main;

import java.awt.Dimension;
import java.awt.Toolkit;

import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MenuPartie extends Stage {
	
	final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	Scene scene;
	public MenuPartie() {
        
        this.setTitle("PacMan");
        this.setMinHeight(625.0);
        this.setMinWidth(775.0);
        this.setX(0.0);
        this.setY(50.0);
        this.scene = new Scene(creerContenu(), dim.width, dim.height, Color.TRANSPARENT);
        this.setFullScreen(true);
        this.setScene(scene);
        this.sizeToScene();
    }
	
	public BorderPane creerContenu() {
        final BorderPane root = new BorderPane();
        root.setBackground(new Background(new BackgroundImage[] { new BackgroundImage(new Image("file:data/assets/images/FondPacMan.png"), BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, new BackgroundPosition(Side.LEFT, 0.0, true, Side.BOTTOM, 0.0, true), new BackgroundSize(-1.0, -1.0, true, true, false, true)) }));
        return root;
    }
}
