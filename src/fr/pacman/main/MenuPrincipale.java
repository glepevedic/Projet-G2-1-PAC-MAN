package fr.pacman.main;

import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderStroke;
import javafx.geometry.Pos;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.Font;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BackgroundPosition;
import javafx.geometry.Side;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BorderPane;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.Node;
import javafx.animation.ScaleTransition;
import java.awt.Dimension;
import javafx.scene.paint.Paint;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import java.awt.Toolkit;
import javax.sound.sampled.Clip;
import javafx.scene.control.Button;
import javafx.scene.text.TextFlow;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MenuPrincipale extends Stage {
	private Text titre;
	private Text titre2;
	private TextFlow titreFlow;
	private Button jouer;
	private Button tutoriel;
	private Button editeur;
	private Button quitter;
	final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

	private ImageView son = Fenetre.imageSon;
	private ImageView ecran = Fenetre.imageEcran;

	private Clip hover = SoundLoader.loadSound("MenuHover.wav");
	private Clip select = SoundLoader.loadSound("MenuSelect.wav");

	Scene scene;

	public MenuPrincipale() {
		this.titre = new Text("PAC");
		this.titre2 = new Text("man");
		this.titreFlow = new TextFlow();
		this.jouer = new Button("JOUER");
		this.tutoriel = new Button("TUTORIEL");
		this.editeur = new Button("EDITEUR");
		this.quitter = new Button("QUITTER");

		this.setTitle("PacMan");
		this.setMinHeight(625.0);
		this.setMinWidth(775.0);
		this.setX(0.0);
		this.setY(50.0);
		this.scene = new Scene(creerContenu(), dim.width, dim.height, Color.WHITE);
		this.setFullScreen(true);
		this.setScene(scene);
		this.sizeToScene();
	}

	private ScaleTransition scaleIn(final Button btn) {
		final ScaleTransition StIn = new ScaleTransition();
		StIn.setNode((Node) btn);
		StIn.setToX(1.1);
		StIn.setToY(1.1);
		StIn.setDuration(new Duration(50.0));
		StIn.setCycleCount(1);
		return StIn;
	}

	private ScaleTransition scaleOut(final Button btn) {
		final ScaleTransition StOut = new ScaleTransition();
		StOut.setNode((Node) btn);
		StOut.setToX(1.0);
		StOut.setToY(1.0);
		StOut.setDuration(new Duration(50.0));
		StOut.setCycleCount(1);
		return StOut;
	}

	public synchronized void startClip(Clip clip) {
		clip.stop();
		clip.setFramePosition(0);
		clip.start();
	}

	private void gererClick(final ActionEvent e) {
		if (e.getSource() == this.jouer) {
			startClip(select);
			Fenetre.jouer();

		} else if (e.getSource() == this.tutoriel) {
			startClip(select);
			Fenetre.tutoriel();
		} else if (e.getSource() == this.editeur) {
			startClip(select);
			Fenetre.editeur();
		} else if (e.getSource() == this.quitter) {
			startClip(select);
			System.exit(0);
		}
	}

	public BorderPane creerContenu() {
		final BorderPane root = new BorderPane();
		root.setBackground(new Background(
				new BackgroundImage[] { new BackgroundImage(new Image("file:data/assets/images/FondPacMan.png"),
						BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT,
						new BackgroundPosition(Side.LEFT, 0.0, true, Side.BOTTOM, 0.0, true),
						new BackgroundSize(-1.0, -1.0, true, true, false, true)) }));

		final Font Pacfont = Font.loadFont("file:data/assets/fonts/PAC-FONT.TTF", 130.0);
		final Font emulogic = Font.loadFont("file:data/assets/fonts/emulogic.ttf", 35.0);

		this.titreFlow.getChildren().addAll(titre, titre2);
		this.titre.setFill((Paint) Color.YELLOW);
		this.titre.setFont(Pacfont);
		this.titre2.setFill((Paint) Color.RED);
		this.titre2.setFont(Pacfont);
		this.titreFlow.setTextAlignment(TextAlignment.CENTER);

		son.setFitHeight(75);
		son.setFitWidth(80);
		son.setPreserveRatio(true);
		son.setOnMouseClicked(e -> Fenetre.muteMusic());
		BorderPane.setAlignment(this.titreFlow, Pos.TOP_CENTER);
		
		
		
		ecran.setFitHeight(75);
		ecran.setFitWidth(80);
		ecran.setPreserveRatio(true);
		
		ecran.setOnMouseClicked(e -> Fenetre.ToggleFullScreen());
		
		BorderPane.setAlignment(this.titreFlow, Pos.TOP_CENTER);
		
		HBox imageBox = new HBox(20.0);
		imageBox.getChildren().addAll(son, ecran);
		root.setBottom(imageBox);
		this.jouer.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint) Color.BLUE,
				BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
		this.jouer.setTextFill((Paint) Color.WHITE);
		this.jouer.setMaxWidth(400.0);
		this.jouer.setFont(emulogic);
		this.jouer.setStyle("-fx-background-color: transparent;");
		this.jouer.setScaleX(1.0);
		this.jouer.setScaleY(1.0);
		this.jouer.setOnMouseEntered(e -> {
			this.scaleIn(this.jouer).playFromStart();
			startClip(hover);
		});
		this.jouer.setOnMouseExited(e -> this.scaleOut(this.jouer).playFromStart());
		this.jouer.setOnAction(e -> this.gererClick(e));

		this.tutoriel.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint) Color.BLUE,
				BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
		this.tutoriel.setTextFill((Paint) Color.WHITE);
		this.tutoriel.setMaxWidth(400.0);
		this.tutoriel.setFont(emulogic);
		this.tutoriel.setStyle("-fx-background-color: transparent;");
		this.tutoriel.setScaleX(1.0);
		this.tutoriel.setScaleY(1.0);
		this.tutoriel.setOnMouseEntered(e -> {
			this.scaleIn(this.tutoriel).playFromStart();
			startClip(hover);
		});
		this.tutoriel.setOnMouseExited(e -> this.scaleOut(this.tutoriel).playFromStart());
		this.tutoriel.setOnAction(e -> this.gererClick(e));

		this.editeur.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint) Color.BLUE,
				BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
		this.editeur.setTextFill((Paint) Color.WHITE);
		this.editeur.setMaxWidth(400.0);
		this.editeur.setFont(emulogic);
		this.editeur.setStyle("-fx-background-color: transparent;");
		this.editeur.setScaleX(1.0);
		this.editeur.setScaleY(1.0);
		this.editeur.setOnMouseEntered(e -> {
			this.scaleIn(this.editeur).playFromStart();
			startClip(hover);
		});
		this.editeur.setOnMouseExited(e -> this.scaleOut(this.editeur).playFromStart());
		this.editeur.setOnAction(e -> this.gererClick(e));

		this.quitter.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint) Color.BLUE,
				BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
		this.quitter.setTextFill((Paint) Color.WHITE);
		this.quitter.setMaxWidth(400.0);
		this.quitter.setFont(emulogic);
		this.quitter.setStyle("-fx-background-color: transparent;");
		this.quitter.setScaleX(1.0);
		this.quitter.setScaleY(1.0);
		this.quitter.setOnMouseEntered(e -> {
			this.scaleIn(this.quitter).playFromStart();
			startClip(hover);
		});
		this.quitter.setOnMouseExited(e -> this.scaleOut(this.quitter).playFromStart());
		this.quitter.setOnAction(e -> this.gererClick(e));

		final VBox vbox = new VBox(50.0);
		vbox.getChildren().addAll(this.titreFlow);
		VBox.setMargin((Node) this.titreFlow, new Insets(60.0, 0.0, 0.0, 0.0));
		final VBox BoxButton = new VBox(30.0);
		BoxButton.getChildren().addAll(this.jouer, this.tutoriel, this.editeur, this.quitter);
		vbox.setAlignment(Pos.CENTER);
		BoxButton.setAlignment(Pos.CENTER);
		root.setTop((Node) vbox);
		root.setCenter((Node) BoxButton);
		return root;
	}

}
