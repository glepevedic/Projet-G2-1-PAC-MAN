package fr.pacman.main;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundLoader {

public static Clip loadSound(String path) {

    try {
        URL url = SoundLoader.class.getResource(path);
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
        AudioFormat format = audioIn.getFormat();

        DataLine.Info info = new DataLine.Info(Clip.class, format);
        Clip clip = (Clip)AudioSystem.getLine(info);
        clip.open(audioIn);

        return clip;
    } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
        e.printStackTrace();
        return null;
    }
}   
}