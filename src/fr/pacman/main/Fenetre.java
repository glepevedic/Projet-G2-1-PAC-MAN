package fr.pacman.main;

/**
 * Cette Classe affiche les elements du menu de la fenetre principal.
 * @author mathe
 */

import javax.swing.JFrame;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.Media;

@SuppressWarnings("serial")
public class Fenetre extends JFrame {

	public static JFXPanel fxPanel;
	private static Boolean mute = false;
	private static Boolean fullscreen = true;
	private static MediaPlayer music;
	private static Image SonUnmute;
	private static Image SonMute;
	private static Image fullscreenI;
	private static Image windowed;
	public static ImageView imageSon = new ImageView(SonUnmute);
	public static ImageView imageEcran = new ImageView(windowed);
	

	public Fenetre() {

		super("Pac-Man");
		fxPanel = new JFXPanel();
		this.setSize(555, 555);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setUndecorated(fullscreen);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Fenetre.SonUnmute = new Image("file:data/assets/images/HautParleur.png");
		Fenetre.SonMute = new Image("file:data/assets/images/HautParleurMute.png");
		Fenetre.fullscreenI = new Image("file:data/assets/images/Fullscreen.png");
		Fenetre.windowed = new Image("file:data/assets/images/Windowed.png");
		imageSon.setImage(SonUnmute);
		imageEcran.setImage(windowed);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Scene scene = new MenuPrincipale().getScene();
				fxPanel.setScene(scene);
			}
		});
		this.add(fxPanel);
		chargerMusic();
		music.setAutoPlay(true);
		music.setCycleCount(MediaPlayer.INDEFINITE);
		music.play();
	}

	/**
	 * menu jouer
	 */
	public static void jouer() {
		Scene scenefx = new MenuNiveau().getScene();
		fxPanel.setScene(scenefx);
	}

	public JFXPanel getfxPanel() {
		return fxPanel;
	}

	/**
	 * Cette methode permet d'eteindre la musique du jeu.
	 */
	public static void muteMusic() {
		if (mute == false) {
			imageSon.setImage(SonMute);
			music.pause();
			mute = true;
		} else {
			music.play();
			mute = false;
			imageSon.setImage(SonUnmute);
		}
	}

	/**
	 * Cette methode permet de mettre en plein ecran le jeu.
	 */
	public static void ToggleFullScreen() {
		if (fullscreen == false) {
			imageEcran.setImage(fullscreenI);
			fullscreen = true;
			Main.setFullscreen();
			
		} else {
			fullscreen = false;
			Main.setWindowed();
			imageEcran.setImage(windowed);
		}
	}

	/**
	 * cette methode permet un retour au menu principal.
	 */
	public static void retourMenu() {
		Scene scenefx = new MenuPrincipale().getScene();
		fxPanel.setScene(scenefx);
	}

	/**
	 * Cette methode permet d'acceder au tutorial.
	 */
	public static void tutoriel() {
		Scene scenefx = new MenuTutoriel().getScene();
		fxPanel.setScene(scenefx);
	}

	/**
	 * Cette methode charge le fond des menus.
	 */
	public static void ChangerFond() {
		Scene scenefx = new MenuPartie().getScene();
		fxPanel.setScene(scenefx);
	}

	public JFXPanel getJFXPanel() {
		return fxPanel;
	}

	/**
	 * Cette methode permet d'acceder a l'editeur
	 */
	public static void editeur() {
		Scene scenefx = new MenuEditeur().getScene();
		fxPanel.setScene(scenefx);
	}
	
	public static void editeurNiveau() {
		Scene scenefx = new MenuNiveauEditeur().getScene();
		fxPanel.setScene(scenefx);
	}
	
	public static void editeurSaisie() {
		Scene scenefx = new MenuEditeurSaisie().getScene();
		fxPanel.setScene(scenefx);
	}

	public void chargerMusic() {
		String path = "file:///" + System.getProperty("user.dir").replace('\\', '/').replace(" ", "%20") + "/"
				+ "data/assets/audios/musicIntro.mp3";
		System.out.println(path);
		music = new MediaPlayer(new Media(path));
	}
}
