package fr.pacman.main;

import java.awt.Color;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import fr.pacman.background.editeur.EditeurM;
import fr.pacman.background.gestion.FichierCharger;
import fr.pacman.background.partie.Partie;
import fr.pacman.zonedejeux.ElementGraphique;
import fr.pacman.zonedejeux.decor.Map;
import fr.pacman.zonedejeux.decor.Niveau;

public class Main {
	public static final int tailleJeuxX = Map.longueurMap * ElementGraphique.taille;
	public static final int tailleJeuxY = Map.largeurMap * ElementGraphique.taille;

	private static Fenetre fenetre;
	private static EditeurM editeur;
	static Partie partie;

	public static void main(String[] args) throws Exception {
		chargementNiveau();
		fenetre = new Fenetre();
	}

	public static void lancerPartie(Niveau niveau) {
		partie = new Partie(niveau.getId());
		Fenetre.ChangerFond();
		fenetre.add(partie);
		partie.setFocusable(true);
		partie.requestFocusInWindow();
		fenetre.getfxPanel().setFocusable(false);
		fenetre.getfxPanel().setOpaque(false);
		fenetre.remove(fenetre.getfxPanel());
		fenetre.setUndecorated(true);
		fenetre.setExtendedState(JFrame.MAXIMIZED_BOTH);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setVisible(true);
		fenetre.pack();

	}

	public static void recommencer() {
		fenetre.remove(partie);
		partie=new Partie(partie.getNiveauJouer().getId());
		fenetre.add(partie);
		Partie.ToggleMenu(false);
		partie.setFocusable(true);
		partie.requestFocusInWindow();
		fenetre.getfxPanel().setFocusable(false);
		fenetre.getfxPanel().setOpaque(false);
		fenetre.remove(fenetre.getfxPanel());
		fenetre.setUndecorated(true);
		fenetre.setExtendedState(JFrame.MAXIMIZED_BOTH);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setVisible(true);
		fenetre.pack();
	}

	public static void reprendre() {
		Partie.ToggleMenu(false);
		partie.setFocusable(true);
		partie.requestFocusInWindow();
		Partie.start = true;
		Partie.timer.start();
	}

	public static void setFullscreen() {
		fenetre.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
	}

	public static void setWindowed() {
		fenetre.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
	}

	public static void lancerEditeur(Niveau niveau) {
		try {
			editeur = (new EditeurM(niveau.getId()));
			Fenetre.ChangerFond();
			editeur.setFocusable(true);
			fenetre.setFocusable(false);
			fenetre.add(editeur);
			editeur.setBackground(new Color(0, 0, 0, 0));
			// fenetre.setUndecorated(true);
			fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fenetre.getJFXPanel().setFocusable(false);
			fenetre.setVisible(true);
			editeur.setOpaque(false);
			fenetre.setComponentZOrder(editeur, 0);
			fenetre.setComponentZOrder(fenetre.getJFXPanel(), 2);
			fenetre.pack();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void lancerEditeurNouveau(String nom) {

		try {
			editeur = (new EditeurM(nom, true));
			Fenetre.ChangerFond();
			editeur.setFocusable(true);
			fenetre.setFocusable(false);
			fenetre.add(editeur);
			editeur.requestFocusInWindow();
			editeur.setBackground(new Color(0, 0, 0, 0));
			fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fenetre.getJFXPanel().setFocusable(false);
			fenetre.setVisible(true);
			editeur.setOpaque(false);
			fenetre.setComponentZOrder(editeur, 0);
			fenetre.setComponentZOrder(fenetre.getJFXPanel(), 1);
			fenetre.pack();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void fermerEditeur() {
		fenetre = new Fenetre();
	}

	public static void quitter() {
		fenetre.getfxPanel().setScene(new MenuPrincipale().getScene());
		fenetre.add(fenetre.getfxPanel());
		fenetre.getfxPanel().setFocusable(true);
		partie.requestFocusInWindow();
		fenetre.remove(partie);
	}

	public static void chargementNiveau() {
		try {
			FichierCharger.chargerNiveau("./data/niveau/defaut/niveauvierge.pac");
			File repertoire = new File("./data/niveau");
			String liste[] = repertoire.list();

			if (liste != null) {
				for (int i = 0; i < liste.length; i++) {
					if (liste[i].matches("^niveau.*")) {
						System.out.println(liste[i]);
						FichierCharger.chargerNiveau("data/niveau/" + liste[i]);
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		try {
			FichierCharger.changementJoueur("./data/joueur/joueur2.pac");

		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println(FichierCharger.getJoueurConecter());
	}

}
