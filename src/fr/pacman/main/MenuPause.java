package fr.pacman.main;

import javafx.scene.layout.VBox;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderStroke;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.Node;
import javafx.animation.ScaleTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import java.awt.Dimension;
import javafx.scene.paint.Paint;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import java.awt.Toolkit;
import javax.sound.sampled.Clip;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class MenuPause extends Stage {
	private Label label;
	private Text titre;
	private Text titre2;
	private TextFlow titreFlow;
	private Button continuer;
	private Button recommencer;
	private Button quitter;
	final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	private ImageView ecran = Fenetre.imageEcran;
	private ImageView son = Fenetre.imageSon;
	
	private Clip hover = SoundLoader.loadSound("MenuHover.wav");
	private Clip select = SoundLoader.loadSound("MenuSelect.wav");

	Scene scene;

	public MenuPause() {
        this.continuer = new Button("continuer");
        this.recommencer = new Button("recommencer");
        this.quitter = new Button("quitter");
        this.label = new Label("Menu");
        this.titre = new Text("PAU");
        this.titre2 = new Text("se");
        this.titreFlow = new TextFlow();
        
        this.setTitle("PacMan");
        this.setMinHeight(625.0);
        this.setMinWidth(775.0);
        this.setX(0.0);
        this.setY(50.0);
        this.scene = new Scene(creerContenu(), dim.width, dim.height, Color.TRANSPARENT);
        this.setFullScreen(true);
        this.setScene(scene);
        this.sizeToScene();
    }
    
    private ScaleTransition scaleIn(final Button btn) {
        final ScaleTransition StIn = new ScaleTransition();
        StIn.setNode((Node)btn);
        StIn.setToX(1.1);
        StIn.setToY(1.1);
        StIn.setDuration(new Duration(50.0));
        StIn.setCycleCount(1);
        return StIn;
    }
    
    private ScaleTransition scaleOut(final Button btn) {
        final ScaleTransition StOut = new ScaleTransition();
        StOut.setNode((Node)btn);
        StOut.setToX(1.0);
        StOut.setToY(1.0);
        StOut.setDuration(new Duration(50.0));
        StOut.setCycleCount(1);
        return StOut;
    }
    
    public synchronized void startClip(Clip clip) {
        clip.stop();
        clip.setFramePosition(0);
        clip.start();
    }
    
    private void gererClick(final ActionEvent e) {
        if (e.getSource() == this.continuer) {
        	startClip(select);
        	Main.reprendre();
        }else if(e.getSource() == this.recommencer) {
        	startClip(select);
        	Main.recommencer();
        }
        else if(e.getSource() == this.quitter) {
        	startClip(select);
        	Main.quitter();
        }
    }
    
    public static void addTextLimiter(final TextField tf, final int maxLength) {
        tf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
                if (tf.getText().length() > maxLength) {
                    String s = tf.getText().substring(0, maxLength);
                    tf.setText(s);
                }
            }
        });
    }
    
    public BorderPane creerContenu() {
        final BorderPane root = new BorderPane();
        root.setStyle("-fx-background-color: transparent;");
        final Font emulogic = Font.loadFont("file:data/assets/fonts/emulogic.ttf", 20.0);
        
        this.label.setTextFill((Paint)Color.WHITE);
        this.label.setFont(emulogic);
        final Font Pacfont = Font.loadFont("file:data/assets/fonts/PAC-FONT.TTF", 130.0);
        
        this.titreFlow.getChildren().addAll(titre, titre2);
        this.titre.setFill((Paint)Color.YELLOW);
        this.titre.setFont(Pacfont);
        this.titre2.setFill((Paint)Color.RED);
        this.titre2.setFont(Pacfont);
        this.titreFlow.setTextAlignment(TextAlignment.CENTER);
        
        son.setFitHeight(75);
		son.setFitWidth(80);
		son.setPreserveRatio(true);
		son.setOnMouseClicked(e -> Fenetre.muteMusic());
		
		ecran.setFitHeight(75);
		ecran.setFitWidth(80);
		ecran.setPreserveRatio(true);
		ecran.setOnMouseClicked(e -> Fenetre.ToggleFullScreen());

		HBox imageBox = new HBox(20.0);
		imageBox.getChildren().addAll(son, ecran);
		root.setBottom(imageBox);
        
        this.continuer.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(5.0)) }));
        this.continuer.setTextFill((Paint)Color.WHITE);
        this.continuer.setMaxWidth(300.0);
        this.continuer.setFont(emulogic);
        this.continuer.setStyle("-fx-background-color: transparent;");
        this.continuer.setScaleX(1.0);
        this.continuer.setScaleY(1.0);
        this.continuer.setOnMouseEntered(e -> {this.scaleIn(this.continuer).playFromStart();startClip(hover);});
        this.continuer.setOnMouseExited(e -> this.scaleOut(this.continuer).playFromStart());
        this.continuer.setOnAction(e -> this.gererClick(e));
       
        this.recommencer.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(5.0)) }));
        this.recommencer.setTextFill((Paint)Color.WHITE);
        this.recommencer.setMaxWidth(300.0);
        this.recommencer.setFont(emulogic);
        this.recommencer.setStyle("-fx-background-color: transparent;");
        this.recommencer.setScaleX(1.0);
        this.recommencer.setScaleY(1.0);
        this.recommencer.setOnMouseEntered(e -> {this.scaleIn(this.recommencer).playFromStart();startClip(hover);});
        this.recommencer.setOnMouseExited(e -> this.scaleOut(this.recommencer).playFromStart());
        this.recommencer.setOnAction(e -> this.gererClick(e));
        
        this.quitter.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(5.0)) }));
        this.quitter.setTextFill((Paint)Color.WHITE);
        this.quitter.setMaxWidth(300.0);
        this.quitter.setFont(emulogic);
        this.quitter.setStyle("-fx-background-color: transparent;");
        this.quitter.setScaleX(1.0);
        this.quitter.setScaleY(1.0);
        this.quitter.setOnMouseEntered(e -> {this.scaleIn(this.quitter).playFromStart();startClip(hover);});
        this.quitter.setOnMouseExited(e -> this.scaleOut(this.quitter).playFromStart());
        this.quitter.setOnAction(e -> this.gererClick(e));
        
        final VBox center = new VBox(50.0);
        center.setAlignment(Pos.CENTER);
        
        final VBox BoxButton = new VBox(30.0);
        BoxButton.getChildren().addAll(this.continuer,this.recommencer, this.quitter);
        BoxButton.setAlignment(Pos.CENTER);
        center.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(50.0), new BorderWidths(7.0)) }));
        center.setStyle("-fx-background-color: black;-fx-background-radius: 55px;");
        center.setMaxWidth(400);
        center.setMaxHeight(400);
        center.getChildren().setAll(this.label,BoxButton);
        
        final VBox vbox = new VBox(50.0);
        vbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(this.titreFlow);
        VBox.setMargin((Node)this.titreFlow, new Insets(60.0, 0.0, 0.0, 0.0));
        
        root.setCenter(center);
        root.setTop(vbox);
        return root;
    }
    
    
}
