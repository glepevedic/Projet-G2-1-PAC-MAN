package fr.pacman.zonedejeux.decor;


import java.util.ArrayList;
import java.util.List;

import fr.exceptionpersonnalisee.ModificationJoueurException;

/**
 * Cette classe d�finis un joueur avec son nom, son id et la liste des records qu'il
 * eu sur diff�rent niveau du jeu.
 * @author gillian
 *
 */
public class Joueur {
	final static int maxElement = 25;
	private final String nomFichier;
	private final String id;
	private final String nom;
	private List<Record> listeRecord;
	
	/**
	 * Constructeur Joueur
	 * 
	 * @param nomFichier
	 * @param id
	 * @param nom
	 */

	public Joueur(String nomFichier, String id, String nom) {
		this.nomFichier = nomFichier;
		this.id = id;
		this.nom = nom;
		listeRecord = new ArrayList<>(maxElement);
	}

	/**
	 * Permet de d'ajouter un record a la liste des record du joueur.
	 * 
	 * @param record
	 * @throws ModificationJoueurException
	 */
	public void ajouterRecord(Record record) throws ModificationJoueurException {
		boolean trouver = false;
		if (record == null) {
			throw new ModificationJoueurException("Record null");
		}

		for (Record i : listeRecord) {
			if (i.getId().equals(record.getId())) {
				i = i.meilleurRecord(record);
				trouver = true;
			}
		}

		if (!trouver) {
			if (this.listeRecord.size() >= maxElement) {
				throw new ModificationJoueurException("Erreur : Liste plein");
			}
			this.listeRecord.add(record);
		}
	}
	
	/**
	 * Permet de supprimer un record de la liste du joueur.
	 * 
	 * @param id
	 * @return le record supprimer
	 * @throws ModificationJoueurException
	 */
	
	public Record suprimerRecord(String id) throws ModificationJoueurException {
		Record recordSupp = null;
		for (Record i : listeRecord) {
			if (i.getId().equals(id)) {
				recordSupp = i;
			}
		}
		
		if (recordSupp == null) {
			throw new ModificationJoueurException("Erreur : record pas trouv�");
		}
		
		listeRecord.remove(recordSupp);
		
		if (listeRecord.contains(recordSupp)) {
			throw new ModificationJoueurException("Erreur : impossible supprimer record");
		}
		
		System.out.println("Record supprimer");
		
		return recordSupp;
	}
	
	/**
	 * 
	 * @return le nom du Fichier
	 */
	public String getNomFichier() {
		return nomFichier;
	}

	/**
	 * 
	 * @return id
 	 */
	public String getId() {
		return id;
	}

	/**
	 * 
	 * @return le nom du joueur.
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * 
	 * @return la liste de tout les record du joueur.
	 */
	public List<Record> getListeRecord() {
		return listeRecord;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listeRecord == null) ? 0 : listeRecord.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((nomFichier == null) ? 0 : nomFichier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Joueur other = (Joueur) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (listeRecord == null) {
			if (other.listeRecord != null)
				return false;
		} else if (!listeRecord.equals(other.listeRecord))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (nomFichier == null) {
			if (other.nomFichier != null)
				return false;
		} else if (!nomFichier.equals(other.nomFichier))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "\nJoueur \n	nomFichier=" + nomFichier + "\n	id=" + id + "\n	nom=" + nom + "\n	listeRecord="
				+ listeRecord + "\n";
	}

}
