package fr.pacman.zonedejeux.decor;

import java.awt.Image;

import fr.exceptionpersonnalisee.BlockException;
import javax.swing.ImageIcon;
import fr.pacman.zonedejeux.ElementGraphique;

/**
 * 
 * @author mathis
 *
 */
public class Block extends ElementGraphique {
	public final static int typeMur = 0;
	public final static int typeSpawn = 1;
	public final static int typeCouloir = 2;
	public final static int typeSuperPacGomme = 3;
	public final static int typePacGommeFruit = 4;
	public final static int typeVerificationMap = -1;

	private int typeblock;

	/**
	 * Constructure Block represente mur, spawn, couloir, superPacgomme,
	 * PacGommeFruit. Definis le type de block par sont numero definis en variable
	 * d'instance.
	 * 
	 * @param coordonnerX
	 * @param coordonnerY
	 * @param texture
	 * @param visible
	 * @param typeblock
	 */
	private Block(float coordonnerX, float coordonnerY, Image texture, boolean visible, int typeblock) {
		super(coordonnerX, coordonnerY, texture, visible);
		this.typeblock = typeblock;
	}

	/**
	 * numero entre -1 et 4 en fonction du type de block.
	 * 
	 * @return typeBlock
	 */
	public int getTypeblock() {
		return typeblock;
	}

	/**
	 * actualise type de block
	 * 
	 * @param typeblock
	 */
	public void setTypeblock(int typeblock) {
		this.typeblock = typeblock;
	}

	/**
	 * revoie l'element graphique du block mur.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public static Block newBlockMur(int x, int y) {
		return new Block(x, y, new ImageIcon("./data/assets/images/mur.png").getImage(), true, Block.typeMur);

	}

	public static Block newBlockMurOrientation(int x, int y, ImageIcon img) {
		return new Block(x, y, img.getImage(), true, Block.typeMur);
	}

	/**
	 * renvoie l'element graphique du block spawn.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public static Block newBlockSpawn(int x, int y) {
		return new Block(x, y, new ImageIcon("./data/assets/images/spawn.png").getImage(), true, Block.typeSpawn);

	}

	/**
	 * renvoie l'element graphique du block couloir.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public static Block newBlockCouloir(int x, int y) {
		return new Block(x, y, new ImageIcon("./data/assets/images/couloir.png").getImage(), true, Block.typeCouloir);

	}

	/**
	 * renvoie l'element graphique du block SuperPacGomme.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public static Block newBlockSuperPacGomme(int x, int y) {
		return new Block(x, y, new ImageIcon("./data/assets/images/superpacgommeblock.png").getImage(), true,
				Block.typeSuperPacGomme);

	}

	/**
	 * renvoie l'element graphique du block PacGommeFruit.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public static Block newBlockPacGommeFruit(int x, int y) {
		return new Block(x, y, new ImageIcon("./data/assets/images/pacgommefruitblock.png").getImage(), true,
				Block.typePacGommeFruit);

	}

	/**
	 * methode qui permet que selon le type du block (entre -1 et 4), affiche
	 * ensuite son element graphique.
	 * 
	 * @param y
	 * @param x
	 * @param type
	 * @return block en fonction de son type.
	 * @throws BlockException
	 */
	public static Block newBlock(int x, int y, int type) throws BlockException {
		Block block = null;
		switch (type) {
		case Block.typeMur:
			block = Block.newBlockMur(x, y);
			break;
		case Block.typeSpawn:
			block = Block.newBlockSpawn(x, y);
			break;
		case Block.typeCouloir:
			block = Block.newBlockCouloir(x, y);
			break;
		case Block.typeSuperPacGomme:
			block = Block.newBlockSuperPacGomme(x, y);
			break;
		case Block.typePacGommeFruit:
			block = Block.newBlockPacGommeFruit(x, y);
			break;

		default:
			throw new BlockException("Erreur : le caractere " + type + " est interdit dans une map");
		}

		return block;
	}

	/**
	 * @param blocksvoisin
	 * @param i
	 * @param y
	 * @return un block soie couloir, un spawn, pacGommeFruit ou super.
	 */
	public boolean estCouloirspawn(Block[][] blocksvoisin, int i, int y) {

		return blocksvoisin[i][y].getTypeblock() == Block.typeCouloir
				|| blocksvoisin[i][y].getTypeblock() == Block.typePacGommeFruit
				|| blocksvoisin[i][y].getTypeblock() == Block.typeSuperPacGomme
				|| blocksvoisin[i][y].getTypeblock() == Block.typeSpawn;

	}

	/**
	 * D�finis l'oriantation des murs de la carte et donne ainsi forme � la map.
	 * @param x
	 * @param y
	 * @param map
	 * @return
	 * @throws BlockException
	 */
	public Block OrientationMur(int x, int y, Map map) throws BlockException {

		Block[][] blocksvoisin;

		// Angles
		if (y == Map.largeurMap - 1 && x == 0) {
			if (map.voisinBasGauche()) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0100.jpg"), 270).getImage());
			} else {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0000.jpg"), 0).getImage());
			}
		} else if (y == Map.largeurMap - 1 && x == Map.longueurMap - 1) {
			if (map.voisinBasDroite()) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_1000.jpg"), 180).getImage());
			} else {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0000.jpg"), 0).getImage());
			}
		} else if (y == 0 && x == Map.longueurMap - 1) {
			if (map.voisinHautDroite()) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0001.jpg"), 90).getImage());
			} else {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0000.jpg"), 0).getImage());
			}
		} else if (y == 0 && x == 0) {
			if (map.voisinHautGauche()) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0010.jpg"), 0).getImage());
			} else {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0000.jpg"), 0).getImage());
			}

			// Bordures sauf Angles
		} else if (y == Map.largeurMap - 1) {
			blocksvoisin = map.voisinHaut(x, y);
			if (estCouloirspawn(blocksvoisin, 0, 1)) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/1000_0000.jpg"), 180).getImage());
			} else if (estCouloirspawn(blocksvoisin, 0, 0) && estCouloirspawn(blocksvoisin, 0, 2)
					&& blocksvoisin[0][1].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_1100.jpg"), 0).getImage());
			} else if (estCouloirspawn(blocksvoisin, 0, 0) && blocksvoisin[0][1].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_1000.jpg"), 180).getImage());
			} else if (estCouloirspawn(blocksvoisin, 0, 2) && blocksvoisin[0][1].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0100.jpg"), 270).getImage());
			} else {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0000.jpg"), 0).getImage());
			}
		} else if (x == Map.longueurMap - 1) {
			blocksvoisin = map.voisinGauche(x, y);
			if (estCouloirspawn(blocksvoisin, 1, 0)) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0100_0000.jpg"), 90).getImage());
			} else if (estCouloirspawn(blocksvoisin, 0, 0) && estCouloirspawn(blocksvoisin, 2, 0)
					&& blocksvoisin[1][0].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_1001.jpg"), 0).getImage());
			} else if (estCouloirspawn(blocksvoisin, 2, 0) && blocksvoisin[1][0].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0001.jpg"), 90).getImage());
			} else if (estCouloirspawn(blocksvoisin, 0, 0) && blocksvoisin[1][0].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_1000.jpg"), 180).getImage());
			} else {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0000.jpg"), 0).getImage());
			}
		} else if (y == 0) {
			blocksvoisin = map.voisinBas(x, y);
			if (estCouloirspawn(blocksvoisin, 2, 1)) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0010_0000.jpg"), 0).getImage());
			} else if (estCouloirspawn(blocksvoisin, 2, 0) && estCouloirspawn(blocksvoisin, 2, 2)
					&& blocksvoisin[2][1].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0011.jpg"), 0).getImage());
			} else if (estCouloirspawn(blocksvoisin, 2, 0) && blocksvoisin[2][1].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0001.jpg"), 90).getImage());
			} else if (estCouloirspawn(blocksvoisin, 2, 2) && blocksvoisin[2][1].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0010.jpg"), 0).getImage());
			} else {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0000.jpg"), 0).getImage());
			}
		} else if (x == 0) {
			blocksvoisin = map.voisinDroite(x, y);
			if (estCouloirspawn(blocksvoisin, 1, 2)) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0100_0000.jpg"), 270).getImage());
			} else if (estCouloirspawn(blocksvoisin, 0, 2) && estCouloirspawn(blocksvoisin, 2, 2)
					&& blocksvoisin[1][2].getTypeblock() == Block.typeMur) {
				// super.setTexture(rotateImageIcon(new
				// ImageIcon("./data/assets/images/0000_0110.jpg"), 0).getImage());
			} else if (estCouloirspawn(blocksvoisin, 2, 2) && blocksvoisin[1][2].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0010.jpg"), 0).getImage());
			} else if (estCouloirspawn(blocksvoisin, 0, 2) && blocksvoisin[1][2].getTypeblock() == Block.typeMur) {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0100.jpg"), 270).getImage());
			} else {
				super.setTexture(rotateImageIcon(new ImageIcon("./data/assets/images/0000_0000.jpg"), 0).getImage());
			}

			// Milieu
		} else {
			blocksvoisin = map.voisin(x, y);
			if (this.getTypeblock() == Block.typeMur) {

				// 0000_quelque chose
				if (blocksvoisin[0][1].getTypeblock() == Block.typeMur
						&& blocksvoisin[1][2].getTypeblock() == Block.typeMur
						&& blocksvoisin[2][1].getTypeblock() == Block.typeMur
						&& blocksvoisin[1][0].getTypeblock() == Block.typeMur) {

					if (blocksvoisin[0][0].getTypeblock() == Block.typeMur
							&& blocksvoisin[0][2].getTypeblock() == Block.typeMur
							&& blocksvoisin[2][2].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_0001.jpg"), 90).getImage());
					} else if (blocksvoisin[0][0].getTypeblock() == Block.typeMur
							&& blocksvoisin[0][2].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 2, 2)
							&& blocksvoisin[2][0].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_0010.jpg"), 0).getImage());
					} else if (blocksvoisin[0][0].getTypeblock() == Block.typeMur
							&& blocksvoisin[0][2].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 2, 2)
							&& estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_0011.jpg"), 0).getImage());
					} else if (blocksvoisin[0][0].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 0, 2)
							&& blocksvoisin[2][2].getTypeblock() == Block.typeMur
							&& blocksvoisin[2][0].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_0100.jpg"), 270).getImage());
					} else if (blocksvoisin[0][0].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 0, 2)
							&& blocksvoisin[2][2].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_0101.jpg"), 180).getImage());
					} else if (blocksvoisin[0][0].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 0, 2)
							&& estCouloirspawn(blocksvoisin, 2, 2)
							&& blocksvoisin[2][0].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_0110.jpg"), 0).getImage());
					} else if (blocksvoisin[0][0].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 0, 2)
							&& estCouloirspawn(blocksvoisin, 2, 2) && estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_0111.jpg"), 0).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0) && blocksvoisin[0][2].getTypeblock() == Block.typeMur
							&& blocksvoisin[2][2].getTypeblock() == Block.typeMur
							&& blocksvoisin[2][0].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_1000.jpg"), 180).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0) && blocksvoisin[0][2].getTypeblock() == Block.typeMur
							&& blocksvoisin[2][2].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_1001.jpg"), 0).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0) && blocksvoisin[0][2].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 2, 2)
							&& blocksvoisin[2][0].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_1010.jpg"), 180).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0) && blocksvoisin[0][2].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 2, 2) && estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_1011.jpg"), 0).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0) && estCouloirspawn(blocksvoisin, 0, 2)
							&& blocksvoisin[2][2].getTypeblock() == Block.typeMur
							&& blocksvoisin[2][0].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_1100.jpg"), 0).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0) && estCouloirspawn(blocksvoisin, 0, 2)
							&& blocksvoisin[2][2].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_1101.jpg"), 0).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0) && estCouloirspawn(blocksvoisin, 0, 2)
							&& estCouloirspawn(blocksvoisin, 2, 2)
							&& blocksvoisin[2][0].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_1110.jpg"), 0).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0) && estCouloirspawn(blocksvoisin, 0, 2)
							&& estCouloirspawn(blocksvoisin, 2, 2) && estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_1111.jpg"), 0).getImage());
					} else {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0000_0000.jpg"), 0).getImage());
					}

					// 0001
				} else if (blocksvoisin[0][1].getTypeblock() == Block.typeMur
						&& blocksvoisin[1][2].getTypeblock() == Block.typeMur
						&& blocksvoisin[2][1].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 1, 0)) {

					if (estCouloirspawn(blocksvoisin, 0, 2) && estCouloirspawn(blocksvoisin, 2, 2)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0001_0110.jpg"), 270).getImage());
					} else if (blocksvoisin[0][2].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 2, 2)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0001_0010.jpg"), 270).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 2)
							&& blocksvoisin[2][2].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0001_0100.jpg"), 270).getImage());
					} else {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0001_0000.jpg"), 90).getImage());
					}

					// 0010
				} else if (blocksvoisin[0][1].getTypeblock() == Block.typeMur
						&& blocksvoisin[1][2].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 2, 1)
						&& blocksvoisin[1][0].getTypeblock() == Block.typeMur) {

					if (estCouloirspawn(blocksvoisin, 0, 0) && estCouloirspawn(blocksvoisin, 0, 2)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0010_1100.jpg"), 180).getImage());
					} else if (blocksvoisin[0][0].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 0, 2)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0010_0100.jpg"), 180).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0)
							&& blocksvoisin[0][2].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0010_1000.jpg"), 180).getImage());
					} else {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0010_0000.jpg"), 0).getImage());
					}

					// 0011
				} else if (blocksvoisin[0][1].getTypeblock() == Block.typeMur
						&& blocksvoisin[1][2].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 2, 1)
						&& estCouloirspawn(blocksvoisin, 1, 0)) {

					if (estCouloirspawn(blocksvoisin, 0, 2)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0011_0100.jpg"), 270).getImage());
					} else {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0011_0000.jpg"), 270).getImage());
					}

					// 0100
				} else if (blocksvoisin[0][1].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 1, 2)
						&& blocksvoisin[2][1].getTypeblock() == Block.typeMur
						&& blocksvoisin[1][0].getTypeblock() == Block.typeMur) {

					if (estCouloirspawn(blocksvoisin, 0, 0) && estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0100_1001.jpg"), 90).getImage());
					} else if (blocksvoisin[0][0].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0100_0001.jpg"), 90).getImage());
					} else if (estCouloirspawn(blocksvoisin, 0, 0)
							&& blocksvoisin[2][0].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0100_1000.jpg"), 90).getImage());
					} else {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0100_0000.jpg"), 270).getImage());
					}

					// 0101
				} else if (blocksvoisin[0][1].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 1, 2)
						&& blocksvoisin[2][1].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 1, 0)) {

					super.setTexture(
							rotateImageIcon(new ImageIcon("./data/assets/images/0101_0000.jpg"), 90).getImage());

					// 0110
				} else if (blocksvoisin[0][1].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 1, 2)
						&& estCouloirspawn(blocksvoisin, 2, 1) && blocksvoisin[1][0].getTypeblock() == Block.typeMur) {

					if (estCouloirspawn(blocksvoisin, 0, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0110_1000.jpg"), 180).getImage());
					} else {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/0110_0000.jpg"), 180).getImage());
					}

					// 0111
				} else if (blocksvoisin[0][1].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 1, 2)
						&& estCouloirspawn(blocksvoisin, 2, 1) && estCouloirspawn(blocksvoisin, 1, 0)) {

					super.setTexture(
							rotateImageIcon(new ImageIcon("./data/assets/images/0111_0000.jpg"), 270).getImage());

					// 1000
				} else if (estCouloirspawn(blocksvoisin, 0, 1) && blocksvoisin[1][2].getTypeblock() == Block.typeMur
						&& blocksvoisin[2][1].getTypeblock() == Block.typeMur
						&& blocksvoisin[1][0].getTypeblock() == Block.typeMur) {

					if (estCouloirspawn(blocksvoisin, 2, 2) && estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/1000_0011.jpg"), 00).getImage());
					} else if (blocksvoisin[2][2].getTypeblock() == Block.typeMur
							&& estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/1000_0001.jpg"), 00).getImage());
					} else if (estCouloirspawn(blocksvoisin, 2, 2)
							&& blocksvoisin[2][0].getTypeblock() == Block.typeMur) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/1000_0010.jpg"), 00).getImage());
					} else {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/1000_0000.jpg"), 180).getImage());
					}

					// 1001
				} else if (estCouloirspawn(blocksvoisin, 0, 1) && blocksvoisin[1][2].getTypeblock() == Block.typeMur
						&& blocksvoisin[2][1].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 1, 0)) {

					if (estCouloirspawn(blocksvoisin, 2, 2)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/1001_0010.jpg"), 00).getImage());
					} else {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/1001_0000.jpg"), 00).getImage());
					}

					// 1010
				} else if (estCouloirspawn(blocksvoisin, 0, 1) && blocksvoisin[1][2].getTypeblock() == Block.typeMur
						&& estCouloirspawn(blocksvoisin, 2, 1) && blocksvoisin[1][0].getTypeblock() == Block.typeMur) {

					super.setTexture(
							rotateImageIcon(new ImageIcon("./data/assets/images/1010_0000.jpg"), 00).getImage());

					// 1011
				} else if (estCouloirspawn(blocksvoisin, 0, 1) && blocksvoisin[1][2].getTypeblock() == Block.typeMur
						&& estCouloirspawn(blocksvoisin, 2, 1) && estCouloirspawn(blocksvoisin, 1, 0)) {

					super.setTexture(
							rotateImageIcon(new ImageIcon("./data/assets/images/1011_0000.jpg"), 00).getImage());

					// 1100
				} else if (estCouloirspawn(blocksvoisin, 0, 1) && estCouloirspawn(blocksvoisin, 1, 2)
						&& blocksvoisin[1][0].getTypeblock() == Block.typeMur
						&& blocksvoisin[2][1].getTypeblock() == Block.typeMur) {

					if (estCouloirspawn(blocksvoisin, 2, 0)) {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/1100_0001.jpg"), 90).getImage());
					} else {
						super.setTexture(
								rotateImageIcon(new ImageIcon("./data/assets/images/1100_0000.jpg"), 90).getImage());
					}

					// 1101
				} else if (estCouloirspawn(blocksvoisin, 0, 1) && estCouloirspawn(blocksvoisin, 1, 2)
						&& blocksvoisin[2][1].getTypeblock() == Block.typeMur && estCouloirspawn(blocksvoisin, 1, 0)) {

					super.setTexture(
							rotateImageIcon(new ImageIcon("./data/assets/images/1101_0000.jpg"), 90).getImage());

					// 1110
				} else if (estCouloirspawn(blocksvoisin, 0, 1) && estCouloirspawn(blocksvoisin, 1, 2)
						&& estCouloirspawn(blocksvoisin, 2, 1) && blocksvoisin[1][0].getTypeblock() == Block.typeMur) {

					super.setTexture(
							rotateImageIcon(new ImageIcon("./data/assets/images/1110_0000.jpg"), 180).getImage());

					// 1111
				} else if (estCouloirspawn(blocksvoisin, 0, 1) && estCouloirspawn(blocksvoisin, 1, 2)
						&& estCouloirspawn(blocksvoisin, 1, 0) && estCouloirspawn(blocksvoisin, 2, 1)) {

					super.setTexture(
							rotateImageIcon(new ImageIcon("./data/assets/images/1111_0000.jpg"), 360).getImage());

				}

				// Ce qui n'est pas un Mur
			} else {

				super.setTexture(new ImageIcon("./data/assets/images/0000_0000.jpg").getImage());

			}
		}
		return null;
	}

	/**
	 * 
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + typeblock;
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Block other = (Block) obj;
		if (typeblock != other.typeblock)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Block [typeblock=" + typeblock + "]";
	}

	/**
	 * @param type
	 * @return Le type de block entre mur, spawn, couloir, pacgommmefruit et superPacGomme.
	 */
	public static String getBlockUrlWithType(int type) {
		String adresse = "";
		if (type == 0) {
			adresse = "mur.png";
		} else if (type == 1) {
			adresse = "spawn.png";
		} else if (type == 2) {
			adresse = "couloir.png";
		} else if (type == 3) {
			adresse = "superpacgommeblock.png";
		} else if (type == 4) {
			adresse = "pacgommefruitblock.png";
		}
		return "./data/assets/images/" + adresse;
	}
}
