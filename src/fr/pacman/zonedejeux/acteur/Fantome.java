package fr.pacman.zonedejeux.acteur;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.ImageIcon;

import fr.pacman.background.partie.Dijkstra;
import fr.pacman.zonedejeux.decor.Block;
import fr.pacman.zonedejeux.decor.Map;

/**
 * Cette class permet de gerer les fantomes. Les fantomes sont des
 * personnages. Ils sont au nombre de 4.
 * 
 * @author Charly
 *
 */

public abstract class Fantome extends Personnage {
	protected boolean libre;
	private int pointFantome = 50;

	/**
	 * 
	 * Constructeur
	 */

	protected Fantome(float coordonnerX, float coordonnerY, Image texture) {
		super(coordonnerX, coordonnerY, texture, true, 0, 1);
		this.libre = false;
	}

	/**
	 * Permet de changer la direction d'un fantome si il n'est pas bloquer au
	 * debut de la partie, et si il se trouve au centre d'une case. 1 correspond
	 * haut, 2 correspond droite, 3 correspond bas, 4 correspond
	 * gauche.
	 * 
	 * @param PAC-MAN, le personnage utilis� par le joueur.
	 * @throws Exception si le fantome ne repond pas au critere.
	 */

	public void deplacement(PacMan pacman, Map map) throws Exception {
		if (estAuCentre()) {
			super.orientation = this.changementDirectionAuto(pacman, map);
		}

		if (isLibre()) {
			super.deplacement(map);
		}

	}

	/**
	 * @return 0, 1, 2 ou 3 en fonction des coordonner des fantomes des leurs spawns.
	 * cela permet de les faire sortir en debut de partie.
	 */
	public int sortieSpawn() {
		if (super.coordonnerX == 14 && super.coordonnerY == 12) {
			return 3;
		}
		if (super.coordonnerX == 16 && super.coordonnerY == 12) {
			return 1;
		}
		if (super.coordonnerX == 15 && super.coordonnerY == 12) {
			return 2;
		}
		if (super.coordonnerX == 15 && super.coordonnerY == 11) {
			return 2;
		}

		return 0;
	}

	/**
	 * @param pacman
	 * @return les coordonners que les fantome doivent avoir pour pouvoir toucher le PacMan.
	 */
	public boolean touche(PacMan pacman) {
		return (pacman.getCoordonnerX() - super.coordonnerX < 0.5 && pacman.getCoordonnerX() - super.coordonnerX > -0.5
				&& pacman.getCoordonnerY() - super.coordonnerY < 0.5
				&& pacman.getCoordonnerY() - super.coordonnerY > -0.5);
	}

	/**
	 * Permet de changer la direction d'un fantome en fonction de son intelligence
	 * artificiel.
	 * 
	 * @param PAC-MAN, le personnage utilis� par le joueur.
	 * @throws Exception si le fantome ne repond pas au critere.
	 */
	protected abstract int changementDirectionAuto(PacMan pacman, Map map);

	/**
	 * @return la texture du fantome
	 */
	public Image remetreTexture() {
		return null;
	}

	/**
	 * @param cheminDijkstra
	 * @param pacman
	 * @return le Chemin que vas prendre de fantome qui utilise l'algorithme de Dijkstra et qui est un algo
	 * qui trouve le chemin le plus court pour arriver le plus rapidement possible au dernier sommet
	 * pointer par le PacMan.
	 * (Donne le chemin calculer par Dijkstra).
	 */
	protected ArrayList<Integer> donneCheminDijkstra(ArrayList<Integer> cheminDijkstra, PacMan pacman) {
		for (int i = 0; i < Dijkstra.sommet.size(); i++) {
			if (super.touche(Dijkstra.sommet.get(i))) {
				cheminDijkstra = Dijkstra.cheminLePlusCour(i, pacman.getDernierSommet());
			}
		}

		return cheminDijkstra;
	}

	/**
	 * @param cheminDijkstra
	 * @param pacman
	 * @return Les futures direction que devra prendre le fantome pour arriver un dernier sommet du
	 * PacMan.
	 */
	protected int futureDirectionDijkstra(ArrayList<Integer> cheminDijkstra, PacMan pacman) {
		for (int i = 0; i < Dijkstra.sommet.size(); i++) {
			if (super.touche(Dijkstra.sommet.get(i))) {
				

				if (cheminDijkstra.size() > 1) {
					Block actuel = Dijkstra.sommet.get(cheminDijkstra.get(0));
					Block future = Dijkstra.sommet.get(cheminDijkstra.get(1));

					if (cheminDijkstra.get(0) > cheminDijkstra.get(1)) {

						if (actuel.getCoordonnerX() == future.getCoordonnerX()) {
							orientation = 2;
						} else {
							orientation = 1;
						}
					} else {
						if (actuel.getCoordonnerX() == future.getCoordonnerX()) {
							orientation = 4;
						} else {
							orientation = 3;
						}
					}
				} else {

					if (super.getCoordonnerX() == pacman.getCoordonnerX()) {
						if (super.coordonnerY > pacman.getCoordonnerY()) {
							orientation = 2;
						} else {
							orientation = 4;
						}
					} else {
						if (super.coordonnerX > pacman.getCoordonnerX()) {
							orientation = 1;
						} else {
							orientation = 3;
						}
					}
				}
				System.out.println(cheminDijkstra);
				cheminDijkstra.remove(0);
			}
		}

		return orientation;
	}

	/**
	 * @param pacman
	 * @param posibiliter
	 * @return Le comportement inverse du PacMan, cette methode est utiliser lorsque le PacMan a manger
	 * un superPacGomme.
	 */
	protected int comportementOppose(PacMan pacman, ArrayList<Integer> posibiliter) {
		int oriantation = 0;

		if (posibiliter.size() >= 2) {
			switch (super.orientation) {
			case 1:
				posibiliter.remove((Integer) 3);
				break;
			case 2:
				posibiliter.remove((Integer) 4);
				break;
			case 3:
				posibiliter.remove((Integer) 1);
				break;
			case 4:
				posibiliter.remove((Integer) 2);
				break;
			default:
				break;
			}
		}

		for (Integer i : prioriter((int) (super.coordonnerX - pacman.getCoordonnerX()),
				(int) (super.coordonnerY - pacman.getCoordonnerY()))) {
			if (posibiliter.contains(i) && oriantation == 0) {
				oriantation = i.intValue();
			}
		}

		return oriantation;
	}

	/**
	 * @param deltaX
	 * @param deltaY
	 * @return la prioriter a prendre.
	 */
	private ArrayList<Integer> prioriter(int deltaX, int deltaY) {

		ArrayList<Integer> prioriter = null;

		if (deltaX > 0 && deltaY > 0) {
			if (deltaY > deltaX) {
				prioriter = new ArrayList<Integer>(Arrays.asList(4, 3, 1, 2));
				// 4 3 1 2
			} else {
				prioriter = new ArrayList<Integer>(Arrays.asList(3, 4, 2, 1));
				// 3 4 2 1
			}
		}

		if (deltaX > 0 && deltaY <= 0) {

			if (deltaY > deltaX) {
				prioriter = new ArrayList<Integer>(Arrays.asList(2, 3, 1, 4));
				// 2 3 1 4
			} else {
				prioriter = new ArrayList<Integer>(Arrays.asList(3, 2, 4, 1));
				// 3 2 4 1
			}
		}

		if (deltaX <= 0 && deltaY > 0) {
			if (deltaY > deltaX) {
				prioriter = new ArrayList<Integer>(Arrays.asList(4, 1, 3, 2));
				// 4 1 3 2
			} else {
				prioriter = new ArrayList<Integer>(Arrays.asList(1, 4, 2, 3));
				// 1 4 2 3
			}
		}
		if (deltaX <= 0 && deltaY <= 0) {
			if (deltaY > deltaX) {
				prioriter = new ArrayList<Integer>(Arrays.asList(2, 1, 3, 4));
				// 2 1 3 4
			} else {
				prioriter = new ArrayList<Integer>(Arrays.asList(1, 2, 4, 3));
				// 1 2 4 3
			}
		}

		return prioriter;
	}

	/**
	 * @return libre
	 */
	public boolean isLibre() {
		return libre;
	}

	/**
	 * Permet de set a vrai ou faux libre
	 * @param libre
	 */
	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	/**
	 * cette methode definis la texture peur sur les fantomes lorsque le PacMan a manger
	 * un SuperPacGomme.
	 */
	public void peur() {
		super.texture = new ImageIcon("./data/assets/images/peur.png").getImage();

	}

	/**
	 * la texture du fantome est set a false.
	 * @return le nombre de point Fantome lorsque le PacMan manger un fantome en etant en mode super.
	 */
	public int seFaitManger() {
		super.visible = false;

		return this.pointFantome;
	}

}
