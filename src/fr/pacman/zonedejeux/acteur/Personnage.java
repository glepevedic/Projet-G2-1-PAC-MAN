package fr.pacman.zonedejeux.acteur;

/**
 * Cette methode definis les personnages present dans le jeu.
 * @author mathe
 */

import java.awt.Image;

import java.util.ArrayList;

import fr.pacman.zonedejeux.ElementGraphique;
import fr.pacman.zonedejeux.decor.Block;
import fr.pacman.zonedejeux.decor.Map;

public abstract class Personnage extends ElementGraphique {

	public final float VITESSE = (float) 0.040;
	protected int orientation;
	protected int vieRestante;

	/**
	 * Constructure Personnage.
	 * @param coordonnerX
	 * @param coordonnerY
	 * @param texture
	 * @param visible
	 * @param orientation
	 * @param vieRestante
	 */
	public Personnage(float coordonnerX, float coordonnerY, Image texture, boolean visible, int orientation,
			int vieRestante) {
		super(coordonnerX, coordonnerY, texture, visible);
		this.orientation = orientation;
		this.setVieRestante(vieRestante);
	}

	/**
	 * Gere l'ensemble des deplacement des personnages sur la carte. 
	 * @param map
	 */
	public void deplacement(Map map) {

		switch (orientation) {
		case 1:
			super.coordonnerX = (float) (Math.round((super.coordonnerX - VITESSE) * 10000) / 10000.0);

			break;
		case 2:
			super.coordonnerY = (float) (Math.round((super.coordonnerY - VITESSE) * 10000) / 10000.0);
			break;
		case 3:
			super.coordonnerX = (float) (Math.round((super.coordonnerX + VITESSE) * 10000) / 10000.0);
			break;
		case 4:
			super.coordonnerY = (float) (Math.round((super.coordonnerY + VITESSE) * 10000) / 10000.0);
			break;
		default:
			break;
		}

	}

	/**
	 * @return True ou False si le personnage est situer aux centre. 
	 */
	public boolean estAuCentre() {
		boolean centre = false;
		if (super.getCoordonnerX() == (int) super.getCoordonnerX()
				&& super.getCoordonnerY() == (int) super.getCoordonnerY()) {
			centre = true;
		}
		return centre;

	}

	/**
	 * Cette methode gere le tableau de direction des personnage. 
	 * @param tabvoisin 
	 * @return tab 
	 */
	public ArrayList<Integer> directionPossible(Block[][] tabvoisin) {
		ArrayList<Integer> tab = new ArrayList<>();

		if (tabvoisin[1][0].getTypeblock() > Block.typeSpawn) {
			tab.add(1);
		}
		if (tabvoisin[0][1].getTypeblock() > Block.typeSpawn) {
			tab.add(2);
		}
		if (tabvoisin[1][2].getTypeblock() > Block.typeSpawn) {
			tab.add(3);
		}
		if (tabvoisin[2][1].getTypeblock() > Block.typeSpawn) {
			tab.add(4);
		}

		return tab;
	}

	/**
	 * @return Oriantation du Personnage.
	 */
	public int getOrientation() {
		return this.orientation;
	}

	/**
	 * Permet de set l'Oriantation du Personnage.
	 * @param orientation
	 */
	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	/**
	 * @return Le nombre de vie restant.
	 */
	public int getVieRestante() {
		return vieRestante;
	}

	/**
	 * Permet de set le nombre de vie.
	 * @param vieRestante
	 */
	public void setVieRestante(int vieRestante) {
		this.vieRestante = vieRestante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + orientation;
		result = prime * result + vieRestante;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personnage other = (Personnage) obj;
		if (orientation != other.orientation)
			return false;
		if (vieRestante != other.vieRestante)
			return false;
		return true;
	}

}
