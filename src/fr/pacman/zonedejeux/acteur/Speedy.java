package fr.pacman.zonedejeux.acteur;

/**
 * Cette class permet de g�rer le fantome Speedy. Ce fantome essayera de prendre en embuscade PacMan
 * et utilise l'algorithme de Dijkstra.
 * 
 * @author Charly
 *
 */
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import fr.pacman.zonedejeux.decor.Map;

public class Speedy extends Fantome {
	ArrayList<Integer> cheminDijkstra;

	public Speedy() {
		super(15, 12, new ImageIcon("./data/assets/images/speedy.png").getImage());
	}

	/**
	 * Cette methode definis la direction de Speedy.
	 * @return oriantation
	 */
	@Override
	public int changementDirectionAuto(PacMan pacman, Map map) {
		int oriantation = 0;

		oriantation = super.sortieSpawn();

		if (oriantation != 0) {
			return oriantation;
		}
		if (pacman.isModeSuper()) {
			return comportementOppose(pacman,
					super.directionPossible(map.voisin((int) super.coordonnerX, (int) super.coordonnerY)));
		}

		oriantation = super.futureDirectionDijkstra(super.donneCheminDijkstra(cheminDijkstra, pacman),  pacman);

		return oriantation;
	}

	/**
	 * @return la texture de Speedy.
	 */
	public Image remetreTexture() {
		return new ImageIcon("./data/assets/images/speedy.png").getImage();
	}
}
