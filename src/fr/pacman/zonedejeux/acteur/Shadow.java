/**
 * 
 */
package fr.pacman.zonedejeux.acteur;

import java.awt.Image;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

import javax.swing.ImageIcon;

import fr.pacman.zonedejeux.decor.Map;

/**
 * Cette class permet de g�rer le fantome Shadow. Ce fantome suit le m�me
 * parcours que PAC-MAN avec quelques cases de retard.
 * 
 * @author Charly
 *
 */
public class Shadow extends Fantome {

	/**
	 * Liste des d�placements de PAC-MAN
	 */
	private Deque<Integer> saveOrientationPacMac;

	/**
	 * Constructeur de Shadow
	 */
	public Shadow() {
		super(14, 12, new ImageIcon("./data/assets/images/shadow.png").getImage() );
		saveOrientationPacMac = new ArrayDeque<>();
	}

	/** 
	 * Cette methode definis la direction de Pockey. 
	 * @return oriantation 
	 */ 
	@Override
	public int changementDirectionAuto(PacMan pacman, Map map) {
		int orientation = 0;
		if (pacman.estAuCentre()) {
			saveOrientationPacMac.offerFirst(pacman.getOrientation());

			orientation = super.sortieSpawn();

			if (orientation != 0) {
				return orientation;
			}

			orientation = comportementTarget(
					super.directionPossible(map.voisin((int) super.coordonnerX, (int) super.coordonnerY)));
		}

		return orientation;
	}

	/**
	 * Permet de changer la direction de Shadow en fonction des d�placements
	 * pr�c�dant de PAC-MAN.
	 * 
	 * 
	 * @param PAC-MAN, le personnage utilis� par le joueur.
	 * @throws Exception si Shadow ne repond pas au critere.
	 */
	public int comportementTarget(ArrayList<Integer> posibiliter) {
		int orientation = 0;

		if (posibiliter.contains(saveOrientationPacMac.peekLast())) {
			orientation = saveOrientationPacMac.pollLast();
		} else {
			saveOrientationPacMac.pollLast();
		}

		return orientation;

	}
	
	/** 
	 * @return la texture de Shadow. 
	 */ 
	public Image remetreTexture() {
		return new ImageIcon("./data/assets/images/shadow.png").getImage();
	}
}
