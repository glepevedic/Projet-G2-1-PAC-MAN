package fr.pacman.zonedejeux;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import fr.pacman.main.Main;
import fr.pacman.zonedejeux.decor.Map;

/**
 * Cette classe gere l'affichage graphique et la position de tout les elements de zonesDeJeux.
 * @author gilli
 *
 */
public abstract class ElementGraphique {
	public final static int taille = 25;
	protected float coordonnerX;
	protected float coordonnerY;
	protected Image texture;
	protected boolean visible;
	
	/**
	 * Constructeur de Elements Graphique.
	 * @param coordonnerX
	 * @param coordonnerY
	 * @param texture
	 * @param visible
	 */
	public ElementGraphique(float coordonnerX, float coordonnerY, Image texture, boolean visible) {
		this.coordonnerX = coordonnerX;
		this.coordonnerY = coordonnerY;
		this.texture = texture;
		this.visible = visible;
	}

	/**
	 * Cette methode affiche l'images texture au coordoner coordonnerX, coordonnery.
	 * si visible est a true.
	 * 
	 * @throws Exception si visible est a false.
	 */
	public void afficher(Graphics graphics, int width,int height) throws Exception {

		if (!this.visible) {
			throw new Exception("Erreur : Est invisible");
		}
	
			
		graphics.drawImage(this.getTexture(),(int) (this.getCoordonnerX() * ElementGraphique.taille) + ( width/2 - Main.tailleJeuxX/2) ,(int)	(this.getCoordonnerY() * ElementGraphique.taille)+ ( height/2 - Main.tailleJeuxY/2)	, null);

	}

	/**
	 * Permet de set les coordonner, pour plus tard bien placer les images.
	 * @param coordonnerX
	 * @param coordonnerY
	 * @throws Exception
	 */
	public void setCoordonner(float coordonnerX, float coordonnerY) throws Exception {
		if (coordonnerX < 0 || coordonnerX > Map.longueurMap) {
			throw new Exception("Erreur coordonner x pas dans le carte");
		}

		if (coordonnerY < 0 || coordonnerY > Map.largeurMap) {
			throw new Exception("Erreur coordonner y pas dans le carte");
		}

		this.coordonnerX = coordonnerX;
		this.coordonnerY = coordonnerY;
	}

	/**
	 * @param texture la texture des elements de zoneDeJeux.
	 */
	public void setTexture(Image texture) {
		this.texture = texture;
	}

	/**
	 * Permet de set la visibiliter a Vrai ou Faux des elements de zoneDeJeux.
	 * @param visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return la coordonner X de l'element Graphique.
	 */
	public float getCoordonnerX() {
		return coordonnerX;
	}

	/**
	 * @return la coordonner Y de l'element Graphique.
	 */
	public float getCoordonnerY() {
		return coordonnerY;
	}

	/**
	 * @return La texture des elements de zoneDeJeux.
	 */
	public Image getTexture() {
		return texture;
	}

	/**
	 * @return Vrai ou Faux si l'elements est visible Graphiquement.
	 */
	public boolean isVisible() {
		return visible;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(coordonnerX);
		result = prime * result + Float.floatToIntBits(coordonnerY);
		result = prime * result + (visible ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ElementGraphique other = (ElementGraphique) obj;
		if (Float.floatToIntBits(coordonnerX) != Float.floatToIntBits(other.coordonnerX))
			return false;
		if (Float.floatToIntBits(coordonnerY) != Float.floatToIntBits(other.coordonnerY))
			return false;
		if (visible != other.visible)
			return false;
		return true;
	}

	/**
	 * @param element
	 * @return Vrai ou Faux si les elements Graphique ce touche.
	 */
	public boolean touche(ElementGraphique element) {
		return (element.getCoordonnerX() == this.coordonnerX && element.getCoordonnerY() == this.coordonnerY);
	}

	/**
	 * @param picture
	 * @param angle
	 * @return La Texture de l'image dans sa bonne rotation. (Utiliser par le PacMan).
	 */
	public static ImageIcon rotateImageIcon(ImageIcon picture, double angle) {
		// FOR YOU ...
		int w = picture.getIconWidth();
		int h = picture.getIconHeight();
		int type = BufferedImage.TYPE_INT_RGB; // other options, see api
		BufferedImage image = new BufferedImage(h, w, type);
		Graphics2D g2 = image.createGraphics();
		double x = (h - w) / 2.0;
		double y = (w - h) / 2.0;
		AffineTransform at = AffineTransform.getTranslateInstance(x, y);
		at.rotate(Math.toRadians(angle), w / 2.0, h / 2.0);
		g2.drawImage(picture.getImage(), at, null);
		g2.dispose();
		picture = new ImageIcon(image);

		return picture;
	}

}
