package fr.pacman.zonedejeux.point;

import javax.swing.ImageIcon;

/**
 * 
 * @author Mathis
 *
 */

public class PacGommeFruit extends PacGomme {
	protected final int pointFruit = 100;

	/**
	 * Constructeur pour les packages exterieur
	 * 
	 * @param coordonnerX
	 * @param coordonnerY
	 */

	public PacGommeFruit(float coordonnerX, float coordonnerY) {
		super(coordonnerX, coordonnerY, new ImageIcon("./data/assets/images/pacgommefruit.png").getImage());
	}

	/**
	 * Pour mettre la visibiliter de l element a false l'orsque le pacMan passe sur
	 * le PacGomme.
	 * 
	 * @return le pointFruit=9 + 1 (de pointpacGomme),
	 * lorsque le pacGommeFruit se fait manger.
	 */

	@Override
	public int seFaitManger() {
		return super.seFaitManger() + this.pointFruit;
	}
}
