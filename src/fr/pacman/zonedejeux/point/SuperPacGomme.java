package fr.pacman.zonedejeux.point;


import javax.swing.ImageIcon;

//import fr.pacman.zonedejeux.*;

/**
 * 
 * @author Mathis
 *
 */

public class SuperPacGomme extends PacGomme {
	private final int pointSuper = 50;	
	/**
	 * Constructeur du SuperPacGomme
	 * 
	 * @param coodonnerX et Y pour la position
	 * @param texture du SuperPacGomme
	 */
	
	public SuperPacGomme(float coordonnerX, float coordonnerY) {
		super(coordonnerX, coordonnerY,  new ImageIcon("./data/assets/images/superpacgomme.png").getImage());
	}
	
	/**
	 * Pour mettre la visibiliter de l element a false l'orsque le pacMan passe sur
	 * le PacGommeSuper.
	 * 
	 * @return le pointSuper=4 + 1 (de pointpacGomme),
	 * lorsque le pacGommeSuper se fait manger.
	 */
	
	@Override
	public int seFaitManger() {
		return super.seFaitManger() + this.pointSuper;
	}
}
