package fr.pacman.zonedejeux.point;

import java.awt.Image;

import javax.swing.ImageIcon;

import fr.pacman.zonedejeux.*;
import fr.pacman.zonedejeux.acteur.PacMan;

/**
 * 
 * @author Mathis Grannec
 *
 */

public class PacGomme extends ElementGraphique {
	protected final int pointPacGomme = 10;
	private boolean manger;

	/**
	 * Constructeur pour les packages exterieur
	 * 
	 * @param coordonnerX
	 * @param coordonnerY
	 */
	public PacGomme(float coordonnerX, float coordonnerY) {
		this(coordonnerX, coordonnerY, new ImageIcon("./data/assets/images/pacgomme.png").getImage());
	}

	/**
	 * Constructeur du PacGomme
	 *  
	 * @param coodonnerX et Y pour la position
	 * @param texture du PacGomme
	 */
	protected PacGomme(float coordonnerX, float coordonnerY, Image texture) {
		super(coordonnerX, coordonnerY, texture, true);
		this.manger = false;
	}

	/**
	 * @return manger
	 */
	public boolean getManger() {
		return manger;
	}

	/**
	 * Pour mettre la visibiliter de l element a false l'orsque le pacMan passe sur
	 * le PacGomme.
	 * 
	 * @return le pointpacgomme=1 lorsque le pacGomme se fait manger.
	 */
	public int seFaitManger() {
		manger = true;
		super.visible = false;
		
		return this.pointPacGomme;
	}
	
	/**
	 * Cette methode score regarde si le PacGomme n'est plus visible et si c'est le cas
	 * @throws Exception si PacGomme pas visible alors incr�mentation de variable x � pointPacGomme
	 * @return x � la classe PointPacGomme pour calculer le score.
	 */
	public boolean touche(PacMan pacman) {
		return (super.coordonnerX == pacman.getCoordonnerX() && super.coordonnerY == pacman.getCoordonnerY());
	}


	
	/*Faire @Override de la classe public int seFaitManger()
	et qui definis la visibilitee des pacGomme a false et l'incrementation 
	du score a�la variable d'instantence(pointSuper)*/
	
}
