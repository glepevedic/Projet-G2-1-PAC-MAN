package fr.exceptionpersonnalisee;

@SuppressWarnings("serial")
public class BlockException extends Exception {
	String valeurImpossible;

	public BlockException() {
	}

	public BlockException(String message) {
		super(message);
	}

	public BlockException(String message, String valeurImpossible) {
		super(message);
		this.valeurImpossible = valeurImpossible;
		
	}
}
