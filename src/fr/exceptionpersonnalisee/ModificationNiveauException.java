package fr.exceptionpersonnalisee;

@SuppressWarnings("serial")
public class ModificationNiveauException extends Exception {
	String valeurImpossible;

	public ModificationNiveauException() {
	}

	public ModificationNiveauException(String message) {
		super(message);
	}

	public ModificationNiveauException(String message, String valeurImpossible) {
		super(message);
		this.valeurImpossible = valeurImpossible;
	}
}
