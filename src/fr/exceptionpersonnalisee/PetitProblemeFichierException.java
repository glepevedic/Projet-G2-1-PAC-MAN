package fr.exceptionpersonnalisee;

@SuppressWarnings("serial")
public class PetitProblemeFichierException extends Exception {
	String nomFichier = "N/C";
	String ligneProbleme = "N/C";

	/**
	 * 
	 * @param nomFichier
	 * @param message
	 */
	public PetitProblemeFichierException(String ligneProbleme, String message) {
		super(message);
		this.ligneProbleme = ligneProbleme;
	}

	/**
	 * 
	 * @param nomFichier
	 * @param ligneProbleme
	 * @param message
	 */
	public PetitProblemeFichierException(String nomFichier, String ligneProbleme, String message) {
		super(message);
		this.nomFichier = nomFichier;
		this.ligneProbleme = ligneProbleme;
	}

	@Override
	public String toString() {
		return "GrosProblemeFichierException [nomFichier=" + nomFichier + ", ligneProbleme=" + ligneProbleme
				+ ", message=" + getMessage() + "]";
	}

}
